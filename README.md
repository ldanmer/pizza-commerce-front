# Pizza Preferita frontend

Project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Build
- Run `npm install` to install dependencies
- Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
- Run `npm start` to start server
## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Live demo
https://pizza-preferita-frontend.herokuapp.com/
