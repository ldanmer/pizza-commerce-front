import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login.component';

/* NgRx */
import { StoreModule } from '@ngrx/store';
import { reducer } from './state/customer.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CustomerEffects } from './state/customer.effects';

@NgModule({
  imports: [
    SharedModule,
    StoreModule.forFeature('customers', reducer),
    EffectsModule.forFeature(
      [ CustomerEffects ]
    ),
  ],
  declarations: [
    LoginComponent
  ]
})
export class CustomerModule { }
