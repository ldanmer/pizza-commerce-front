import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Customer } from './customer';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  private customersUrl = `${ environment.backendUrl }/customers`;

  constructor(private http: HttpClient) {
  }

  getCurrent(email: string): Observable<Customer | Observable<never>> {
    return this.http.get<Customer | Observable<never>>(`${this.customersUrl}/${email}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(err) {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `${ err.error.message }`;
    } else {
      errorMessage = `Server error: ${JSON.stringify(err.error)}`;
    }
    return throwError(errorMessage);
  }
}
