/* NgRx */
import { Action } from '@ngrx/store';
import { Customer } from '../customer';

export enum CustomerActionTypes {
  Load = '[Customer] Load',
  LoadSuccess = '[Customer] Load Success',
  LoadFail = '[Customer] Load Fail',
}

export class Load implements Action {
  readonly type = CustomerActionTypes.Load;

  constructor(public payload: string) {
  }
}

export class LoadSuccess implements Action {
  readonly type = CustomerActionTypes.LoadSuccess;

  constructor(public payload: Customer) {
  }
}

export class LoadFail implements Action {
  readonly type = CustomerActionTypes.LoadFail;

  constructor(public payload: string) {
  }
}

export type CustomerActions = Load
  | LoadSuccess
  | LoadFail;
