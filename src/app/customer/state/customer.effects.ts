import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError, tap } from 'rxjs/operators';

/* NgRx */
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as customerActions from './customer.actions';
import { CustomerService } from '../customer.service';
import { Load } from './customer.actions';

@Injectable()
export class CustomerEffects {

  constructor(private customerService: CustomerService,
              private actions$: Actions) {
  }

  @Effect()
  loadCustomer$: Observable<Action> = this.actions$.pipe(
    ofType<Load>(customerActions.CustomerActionTypes.Load),
    map((action: customerActions.Load) => action.payload),
    mergeMap((email: string) =>
      this.customerService.getCurrent(email).pipe(
        // @ts-ignore
        map(customer => (new customerActions.LoadSuccess(customer))),
        catchError(err => of(new customerActions.LoadFail(err)))
      )
    )
  );
}
