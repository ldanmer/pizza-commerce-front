import { Customer } from '../customer';

/* NgRx */
import { CustomerActions, CustomerActionTypes } from './customer.actions';

export interface CustomerState {
  current: Customer;
  error: string;
}

const initialState: CustomerState = {
  current: null,
  error: ''
};

export function reducer(state = initialState, action: CustomerActions): CustomerState {
  switch (action.type) {
    case CustomerActionTypes.LoadSuccess:
      return {
        ...state,
        current: action.payload,
        error: ''
      };

    case CustomerActionTypes.LoadFail:
      return {
        ...state,
        current: null,
        error: action.payload
      };

    default:
      return state;
  }
}
