/* NgRx */
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CustomerState } from './customer.reducer';
import * as fromRoot from '../../state/app.state';
import * as fromCustomers from './customer.reducer';

export interface State extends fromRoot.State {
  customers: fromCustomers.CustomerState;
}

// Selector functions
const getProductFeatureState = createFeatureSelector<CustomerState>('customers');

export const getCurrentCustomer = createSelector(
  getProductFeatureState,
  state => state.current
);
