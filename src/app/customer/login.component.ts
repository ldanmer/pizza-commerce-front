import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.scss' ]
})
export class LoginComponent implements OnInit{
  pageTitle = 'Log In';
  errorMessage: string;

  constructor(private authService: AuthService, private router: Router) {
  }

  cancel(): void {
    this.router.navigate([ '/' ]);
  }

  login(loginForm: NgForm): void {
    if (loginForm.valid) {
      this.authService.login(loginForm.form.value.email, loginForm.form.value.password);
    } else {
      this.errorMessage = 'Please enter a user name and password.';
    }
  }

  ngOnInit(): void {

  }

}
