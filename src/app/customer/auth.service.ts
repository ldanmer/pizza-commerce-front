import { Injectable } from '@angular/core';
import { Customer } from './customer';
import { Action, ActionsSubject, Store } from '@ngrx/store';
import * as fromCustomer from './state';
import * as customerActions from './state/customer.actions';
import { ofType } from '@ngrx/effects';
import { CustomerActionTypes, LoadSuccess } from './state/customer.actions';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser: Customer;
  redirectUrl: string;

  get isLoggedIn(): boolean {
    return !!this.currentUser;
  }

  constructor(
    private store: Store<fromCustomer.State>,
    private actionsSubject: ActionsSubject,
    private router: Router,
    private notificationService: NotificationsService) {
    this.actionsSubject.pipe(
      ofType<LoadSuccess>(CustomerActionTypes.LoadSuccess),
    ).subscribe(
      (action) => {
        if (!!action.payload) {
          this.currentUser = action.payload;
          this.router.navigate([ '/orders' ]);
        } else {
          this.notificationService.warn('Customer not found');
        }
      }
    );
  }

  login(email: string, password: string): void {
    this.store.dispatch(new customerActions.Load(email));
  }

  logout(): void {
    this.currentUser = null;
  }
}
