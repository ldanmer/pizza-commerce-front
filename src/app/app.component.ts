import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, NavigationStart, RouterEvent } from '@angular/router';
import { State } from './state/app.state';
import { select, Store } from '@ngrx/store';
import * as productActions from './product/state/product.actions';
import { NotificationsService } from 'angular2-notifications';
import { ActionsSubject } from '@ngrx/store';
import { ofType } from '@ngrx/effects';
import { AddProduct, ProductActionTypes } from './product/state/product.actions';
import { CreateOrderSuccess, OrderActionTypes } from './order/state/order.actions';
import { takeWhile } from 'rxjs/operators';
import { Customer } from './customer/customer';

import * as fromCustomer from './customer/state';
import { AuthService } from './customer/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements OnInit, OnDestroy {
  pageTitle = 'Pizza Preferita';
  loading = true;
  phone = '8 800 156 61 80';
  componentActive = true;
  currentCustomer: Customer;

  constructor(
    private router: Router,
    private store: Store<State>,
    private notificationService: NotificationsService,
    private actionsSubject: ActionsSubject,
    private authService: AuthService) {
    router.events.subscribe((routerEvent: RouterEvent) => {
      this.checkRouterEvent(routerEvent);
    });
  }

  ngOnInit(): void {
    this.store.dispatch(new productActions.Load());
    this.actionsSubject.pipe(
      ofType(ProductActionTypes.LoadFail, OrderActionTypes.CreateOrderFail, OrderActionTypes.LoadFail),
      takeWhile(() => this.componentActive)
    ).subscribe(
      error => this.notificationService.error(error)
    );

    this.actionsSubject.pipe(
      ofType<CreateOrderSuccess>(OrderActionTypes.CreateOrderSuccess),
      takeWhile(() => this.componentActive)
    ).subscribe(
      (payload) => {
        this.store.dispatch(new productActions.ClearProductsCart());
        this.notificationService.success('Order successfully created');
        // @ts-ignore
        this.authService.login(payload.payload.email, 'any');
        this.router.navigate([ '/orders' ]);
      }
    );

    this.actionsSubject.pipe(
      ofType<AddProduct>(ProductActionTypes.AddProduct),
      takeWhile(() => this.componentActive)
    ).subscribe(
      () => this.notificationService.success('Product successfully added to cart')
    );

    this.store.pipe(
      select(fromCustomer.getCurrentCustomer),
      takeWhile(() => this.componentActive)
    ).subscribe(
      (current: Customer) => {
        if (!!current) {
          this.currentCustomer = current;
        }
      }
    );
  }

  checkRouterEvent(routerEvent: RouterEvent): void {
    this.loading = routerEvent instanceof NavigationStart;
  }

  ngOnDestroy(): void {
    this.componentActive = false;
  }
}
