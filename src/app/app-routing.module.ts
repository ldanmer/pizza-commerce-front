import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found.component';
import { PlaceholderComponent } from './pages/placeholder/placeholder.component';
import { AuthGuard } from './customer/auth.guard';
import { LoginComponent } from './customer/login.component';


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot([
      {path: '', component: HomeComponent, pathMatch: 'full'},
      {path: 'login', component: LoginComponent},
      {path: 'cart', loadChildren: './cart/cart.module#CartModule'},
      {
        path: 'orders', loadChildren: './order/order.module#OrderModule',
        canActivate: [ AuthGuard ],
      },
      {path: 'placeholder', component: PlaceholderComponent},
      {path: '**', component: PageNotFoundComponent}
    ]),
    CommonModule
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
