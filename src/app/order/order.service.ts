import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Order } from './order';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  private ordersUrl = `${environment.backendUrl}/orders`;

  constructor(private http: HttpClient) {
  }

  getOrders(email: string): Observable<Order[]> {
    return this.http.get<Order[]>(`${this.ordersUrl}/${email}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  createOrder(order: Order): Observable<Order> {
    return this.http.post<Order>(`${ this.ordersUrl }/create`, order)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(err) {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `${ err.error.message }`;
    } else {
      errorMessage = `Server error: ${JSON.stringify(err.error)}`;
    }
    return throwError(errorMessage);
  }
}
