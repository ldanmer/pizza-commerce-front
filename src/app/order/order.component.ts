import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as fromOrder from './state';
import * as fromCustomer from '../customer/state';
import { Order } from './order';
import * as orderActions from './state/order.actions';
import { takeWhile } from 'rxjs/operators';
import * as fromCurrency from '../currency/state';
import { Currency } from '../currency/currency';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: [ './order.component.scss' ]
})
export class OrderComponent implements OnInit, OnDestroy {

  pageTitle = 'Your Orders History';
  orders: Order[];
  componentActive = true;
  currency: Currency;
  imageUrl = environment.imageUrl;

  constructor(private store: Store<fromOrder.State>) {
  }

  ngOnInit(): void {
    this.store.pipe(
      select(fromCustomer.getCurrentCustomer),
      takeWhile(() => this.componentActive)
    ).subscribe(
      (current) => {
        if (!!current) {
          this.store.dispatch(new orderActions.Load(current.email));
        }
      }
    );
    this.store.pipe(
      select(fromOrder.getOrders),
      takeWhile(() => this.componentActive)
    ).subscribe(
      orders => this.orders = orders
    );

    this.store.pipe(
      select(fromCurrency.getCurrentCurrency),
      takeWhile(() => this.componentActive)
    ).subscribe(
      currency => this.currency = currency
    );
  }

  ngOnDestroy(): void {
    this.componentActive = false;
  }

}
