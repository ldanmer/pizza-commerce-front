import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { OrderEffects } from './state/order.effects';
import { reducer } from './state/order.reducer';
import { OrderComponent } from './order.component';
import { CurrencyModule } from '../currency/currency.module';

const orderRoutes: Routes = [
  {path: '', component: OrderComponent}
];

@NgModule({
  declarations: [
    OrderComponent
  ],
  imports: [
    SharedModule,
    StoreModule.forFeature('orders', reducer),
    RouterModule.forChild(orderRoutes),
    EffectsModule.forFeature(
      [ OrderEffects ]
    ),
    RouterModule,
    CurrencyModule,
  ]
})
export class OrderModule { }
