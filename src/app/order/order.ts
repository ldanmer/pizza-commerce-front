import { ProductAdded } from '../product/productAdded';
import { Customer } from '../customer/customer';

export enum OrderStatus {
  PENDING = 'pending',
  APPROVED = 'approved',
  DELIVERED = 'delivered',
  CANCELED = 'canceled'
}

interface Address {
  city: string;
  address: string;
}

export interface Order {
  id?: string;
  deliveryPrice: number;
  sum: number;
  status: OrderStatus;
  address: Address;
  customer: Customer;
  products: ProductAdded[];
  created_at?: Date;
}
