import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from '../../state/app.state';
import * as fromOrders from './order.reducer';

export interface State extends fromRoot.State {
  orders: fromOrders.OrderState;
}

// Selector functions
const getOrderFeatureState = createFeatureSelector<fromOrders.OrderState>('orders');

export const getOrders = createSelector(
  getOrderFeatureState,
  state => state.orders
);

export const getError = createSelector(
  getOrderFeatureState,
  state => state.error
);
