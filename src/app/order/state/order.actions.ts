import { Order } from '../order';
import { Action } from '@ngrx/store';

export enum OrderActionTypes {
  Load = '[Order] Load',
  LoadSuccess = '[Order] Load Success',
  LoadFail = '[Order] Load Fail',
  CreateOrder = '[Order] Create Order',
  CreateOrderSuccess = '[Order] Create Order Success',
  CreateOrderFail = '[Order] Create Order Fail',
}

// Action Creators
export class Load implements Action {
  readonly type = OrderActionTypes.Load;

  constructor(public payload: string) {
  }
}

export class LoadSuccess implements Action {
  readonly type = OrderActionTypes.LoadSuccess;

  constructor(public payload: Order[]) {
  }
}

export class LoadFail implements Action {
  readonly type = OrderActionTypes.LoadFail;

  constructor(public payload: string) {
  }
}

export class CreateOrder implements Action {
  readonly type = OrderActionTypes.CreateOrder;

  constructor(public payload: Order) { }
}

export class CreateOrderSuccess implements Action {
  readonly type = OrderActionTypes.CreateOrderSuccess;

  constructor(public payload: Order) { }
}

export class CreateOrderFail implements Action {
  readonly type = OrderActionTypes.CreateOrderFail;

  constructor(public payload: string) { }
}

// Union the valid types
export type OrderActions = Load
  | LoadSuccess
  | LoadFail
  | CreateOrder
  | CreateOrderSuccess
  | CreateOrderFail;


