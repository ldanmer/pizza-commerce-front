import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';

import { OrderService } from '../order.service';

/* NgRx */
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as orderActions from './order.actions';
import { Order } from '../order';
import { Load } from './order.actions';

@Injectable()
export class OrderEffects {

  constructor(private orderService: OrderService,
              private actions$: Actions) {
  }

  @Effect()
  loadOrders$: Observable<Action> = this.actions$.pipe(
    ofType<Load>(orderActions.OrderActionTypes.Load),
    map((email: orderActions.Load) => email.payload),
    mergeMap((email: string) =>
      this.orderService.getOrders(email).pipe(
        map(orders => (new orderActions.LoadSuccess(orders))),
        catchError(err => of(new orderActions.LoadFail(err)))
      )
    )
  );

  @Effect()
  createOrder$: Observable<Action> = this.actions$.pipe(
    ofType(orderActions.OrderActionTypes.CreateOrder),
    map((action: orderActions.CreateOrder) => action.payload),
    mergeMap((order: Order) =>
      this.orderService.createOrder(order).pipe(
        map(newOrder => (new orderActions.CreateOrderSuccess(newOrder))),
        catchError(err => of(new orderActions.CreateOrderFail(err))
        )
      )
    )
  );
}
