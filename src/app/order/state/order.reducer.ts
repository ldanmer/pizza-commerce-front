import { OrderActionTypes, OrderActions } from './order.actions';
import { Order } from '../order';

export interface OrderState {
  orders: Order[];
  error: string;
}

const initialState: OrderState = {
  orders: [],
  error: ''
};

export function reducer(state = initialState, action: OrderActions): OrderState {

  switch (action.type) {

    case OrderActionTypes.LoadSuccess:
      return {
        ...state,
        orders: action.payload,
        error: ''
      };

    case OrderActionTypes.LoadFail:
      return {
        ...state,
        orders: [],
        error: action.payload
      };

    case OrderActionTypes.CreateOrderSuccess:
      return {
        ...state,
        orders: [...state.orders, action.payload],
        error: ''
      };

    case OrderActionTypes.CreateOrderFail:
      return {
        ...state,
        error: action.payload
      };
    default:
      return state;
  }
}
