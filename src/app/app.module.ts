import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { HomeModule } from './home/home.module';
import { HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { ProductModule } from './product/product.module';
import { CartModule } from './cart/cart.module';
import { CartSmallComponent } from './cart/cart-small/cart-small.component';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PlaceholderComponent } from './pages/placeholder/placeholder.component';
import { OrderModule } from './order/order.module';
import { CustomerModule } from './customer/customer.module';
import { CurrencyModule } from './currency/currency.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    CartSmallComponent,
    PlaceholderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HomeModule,
    HttpClientModule,
    EffectsModule.forRoot([]),
    StoreModule.forRoot({}, {}),
    StoreDevtoolsModule.instrument({
      name: 'Pizza Preferita',
      maxAge: 25,
      logOnly: environment.production
    }),
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot({
      position: [ 'top', 'right' ],
      timeOut: 2500
    }),
    ProductModule,
    CartModule,
    OrderModule,
    CustomerModule,
    CurrencyModule
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
