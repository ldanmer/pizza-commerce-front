import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';

import { ProductListComponent } from './product-list/product-list.component';

/* NgRx */
import { StoreModule } from '@ngrx/store';
import { reducer } from './state/product.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ProductEffects } from './state/product.effects';
import { RouterModule } from '@angular/router';
import { CurrencyModule } from '../currency/currency.module';


@NgModule({
  imports: [
    SharedModule,
    StoreModule.forFeature('products', reducer),
    EffectsModule.forFeature(
      [ ProductEffects ]
    ),
    RouterModule,
    CurrencyModule,
  ],
  exports: [
    ProductListComponent
  ],
  declarations: [
    ProductListComponent,
  ]
})
export class ProductModule { }
