import { Product } from '../product';
import { ProductActions, ProductActionTypes } from './product.actions';
import { ProductAdded } from '../productAdded';

export interface ProductState {
  productsAdded: ProductAdded[];
  products: Product[];
  error: string;
}

const initialState: ProductState = {
  productsAdded: [],
  products: [],
  error: ''
};

export function reducer(state = initialState, action: ProductActions): ProductState {

  switch (action.type) {
    case ProductActionTypes.AddProduct:
      if (state.productsAdded.some(el => el.id === action.payload.id)) {
        return state;
      }
      return {
        ...state,
        productsAdded: [ ...state.productsAdded, action.payload ],
        error: ''
      };

    case ProductActionTypes.LoadSuccess:
      return {
        ...state,
        products: action.payload,
        error: ''
      };

    case ProductActionTypes.LoadFail:
      return {
        ...state,
        products: [],
        error: action.payload
      };

    case ProductActionTypes.RemoveProductAdded:
      return {
        ...state,
        productsAdded: state.productsAdded.filter(product => product.id !== action.payload),
        error: ''
      };

    case ProductActionTypes.ClearProductsCart:
      return {
        ...state,
        productsAdded: [],
        error: ''
      };

    default:
      return state;
  }
}
