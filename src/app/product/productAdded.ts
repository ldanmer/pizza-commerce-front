import { Product } from './product';

export interface ProductAdded extends Product{
  amount: number;
}
