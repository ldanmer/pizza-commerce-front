/* Defines the product entity */
export interface Product {
  id: string;
  name: string;
  image: string;
  base_price: number;
  description: string;
}
