import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Product } from './product';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private productsUrl = `${ environment.backendUrl }/pizzas`;

  constructor(private http: HttpClient) {
  }

  getProducts(): Observable<Product[] | Observable<never>> {
    return this.http.get<Product[]>(this.productsUrl)
      .pipe(
        tap(data => console.log(JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  private handleError(err) {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `${ err.error.message }`;
    } else {
      errorMessage = `Server error: ${JSON.stringify(err.error)}`;
    }
    return throwError(errorMessage);
  }
}
