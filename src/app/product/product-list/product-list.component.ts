import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import * as productActions from '../state/product.actions';
import * as fromProduct from '../state';
import * as fromCurrency from '../../currency/state';
import { Product } from '../product';
import { ProductAdded } from '../productAdded';
import { Currency } from '../../currency/currency';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: [ './product-list.component.scss' ]
})
export class ProductListComponent implements OnInit, OnDestroy {
  pageTitle = 'Pizza';
  products$: Observable<Product[]>;
  addedProducts: ProductAdded[];
  componentActive = true;
  currency: Currency;
  imageUrl = environment.imageUrl;

  constructor(private store: Store<fromProduct.State>) {
  }

  ngOnInit(): void {
    this.products$ = this.store.pipe(select(fromProduct.getProducts));
    this.store.pipe(
      select(fromProduct.getProductsAdded),
      takeWhile(() => this.componentActive)
    ).subscribe(
      addedProducts => this.addedProducts = addedProducts
    );

    this.store.pipe(
      select(fromCurrency.getCurrentCurrency),
      takeWhile(() => this.componentActive)
    ).subscribe(
      currency => this.currency = currency
    );
  }

  productAddToCart(product: Product, amount: number): void {
    this.store.dispatch(new productActions.AddProduct({...product, amount}));
  }

  minusCount(count): void {
    count.value = +count.value - 1;
  }

  plusCount(count): void {
    count.value = +count.value + 1;
  }

  ngOnDestroy(): void {
    this.componentActive = false;
  }

  isProductAdded = (product: Product): boolean => this.addedProducts.some(el => el.id === product.id);

  getAmount = (product: Product): number => this.addedProducts.find(added => added.id === product.id)?.amount || 1;
}
