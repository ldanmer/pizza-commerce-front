import { Component, OnInit, OnDestroy } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { takeWhile } from 'rxjs/operators';
import { Router } from '@angular/router';
import * as productActions from '../state/product.actions';
import * as fromProduct from '../state';
import { Product } from '../product';

@Component({
  selector: 'app-slider',
  templateUrl: './product-slider.component.html',
  styleUrls: [ './product-slider.component.scss' ]
})
export class ProductSliderComponent implements OnInit, OnDestroy {
  sliderProduct: Product;
  componentActive = true;

  constructor(private store: Store<fromProduct.State>, private router: Router) {
  }

  ngOnInit(): void {
    this.store.pipe(
      select(fromProduct.getProducts),
      takeWhile(() => this.componentActive)
    ).subscribe(
      products => this.sliderProduct = products[0]
    );
  }

  ngOnDestroy(): void {
    this.componentActive = false;
  }

  productAddToCart(sliderProduct: Product) {
    this.store.dispatch(new productActions.AddProduct({...sliderProduct, amount: 1}));
    this.router.navigate([ '/cart' ]);
  }
}
