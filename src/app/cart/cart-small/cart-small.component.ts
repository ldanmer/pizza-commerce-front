import { Component, OnInit } from '@angular/core';
import * as fromProduct from '../../product/state';
import { select, Store } from '@ngrx/store';
import { ProductAdded } from '../../product/productAdded';

@Component({
  selector: 'app-cart-small',
  templateUrl: './cart-small.component.html',
  styleUrls: [ './cart-small.component.scss' ]
})
export class CartSmallComponent implements OnInit {

  constructor(private store: Store<fromProduct.State>) {
  }

  addedProducts: ProductAdded[];

  ngOnInit(): void {
    this.store.pipe(
      select(fromProduct.getProductsAdded),
    ).subscribe(
      addedProducts => this.addedProducts = addedProducts
    );
  }
}
