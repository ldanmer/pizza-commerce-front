import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { CartComponent } from './cart.component';
import { CurrencyModule } from '../currency/currency.module';

const cartRoutes: Routes = [
  {path: '', component: CartComponent}
];

@NgModule({
  declarations: [
    CartComponent
  ],
    imports: [
        SharedModule,
        RouterModule.forChild(cartRoutes),
        CommonModule,
        CurrencyModule,
    ],
  exports: []
})
export class CartModule {
}
