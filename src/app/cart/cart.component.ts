import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ProductAdded } from '../product/productAdded';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Order, OrderStatus } from '../order/order';
import { debounceTime, takeWhile } from 'rxjs/operators';

import * as fromProduct from '../product/state';
import * as fromOrder from '../order/state';
import * as productActions from '../product/state/product.actions';
import * as orderActions from '../order/state/order.actions';
import { NotificationsService } from 'angular2-notifications';
import { AuthService } from '../customer/auth.service';
import { Currency } from '../currency/currency';
import * as fromCurrency from '../currency/state';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: [ './cart.component.scss' ],
})
export class CartComponent implements OnInit, OnDestroy {
  productsAdded: ProductAdded[];
  totalSum: number;
  order: Order;
  orderForm: FormGroup;
  emailError: string;
  phoneError: string;
  nameError: string;
  cityError: string;
  addressError: string;
  componentActive = true;
  currency: Currency;
  imageUrl = environment.imageUrl;

  private validationMessages = {
    required: 'This field is required',
    email: 'Please enter a valid email address.',
    pattern: 'You are using invalid characters',
    minlength: 'Value too small',
  };

  constructor(private store: Store<fromOrder.State>, private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.store.pipe(
      select(fromProduct.getProductsAdded),
      takeWhile(() => this.componentActive)
    ).subscribe(
      products => {
        this.productsAdded = products;
        this.totalSum = products.reduce((previousValue, currentValue) => previousValue + currentValue.amount * currentValue.base_price, 0);
      }
    );

    this.store.pipe(
      select(fromCurrency.getCurrentCurrency),
      takeWhile(() => this.componentActive)
    ).subscribe(
      currency => this.currency = currency
    );

    this.orderForm = this.fb.group({
      customer: this.fb.group({
        email: [ '', [ Validators.required, Validators.email ] ],
        phone: [ '', [ Validators.required, Validators.pattern(/\+?[\d\s-]+/), Validators.minLength(6) ] ],
        name: [ '', [ Validators.required, Validators.pattern(/[a-zA-Z\s-]+/), Validators.minLength(3) ] ],
      }),
      address: this.fb.group({
        city: [ '', [ Validators.required, Validators.minLength(2) ] ],
        address: [ '', [ Validators.required, Validators.minLength(8) ] ],
      }),
    });

    const emailControl = this.orderForm.get('customer.email');
    emailControl.valueChanges.pipe(
      debounceTime(500)
    ).subscribe(
      value => this.emailError = this.setMessage(emailControl)
    );

    const phoneControl = this.orderForm.get('customer.phone');
    phoneControl.valueChanges.pipe(
      debounceTime(500)
    ).subscribe(
      value => this.phoneError = this.setMessage(phoneControl)
    );
    const nameControl = this.orderForm.get('customer.name');
    nameControl.valueChanges.pipe(
      debounceTime(500)
    ).subscribe(
      value => this.nameError = this.setMessage(nameControl)
    );
    const cityControl = this.orderForm.get('address.city');
    cityControl.valueChanges.pipe(
      debounceTime(500)
    ).subscribe(
      value => this.cityError = this.setMessage(cityControl)
    );
    const addressControl = this.orderForm.get('address.address');
    addressControl.valueChanges.pipe(
      debounceTime(500)
    ).subscribe(
      value => this.addressError = this.setMessage(addressControl)
    );

  }

  ngOnDestroy(): void {
    this.componentActive = false;
  }

  removeProductFromCart(product: ProductAdded) {
    this.store.dispatch(new productActions.RemoveProductAdded(product.id));
  }

  formSubmit() {
    if (this.orderForm.valid) {
      this.order = {
        sum: this.totalSum,
        deliveryPrice: Math.floor((Math.random() * 100) + 1), // for demo purposes - just a random number for delivery price
        products: this.productsAdded,
        status: OrderStatus.PENDING,
        address: {
          city: this.orderForm.get('address.city').value,
          address: this.orderForm.get('address.address').value,
        },
        customer: {
          phone: this.orderForm.get('customer.phone').value,
          email: this.orderForm.get('customer.email').value,
          name: this.orderForm.get('customer.name').value,
        }
      };
      this.store.dispatch(new orderActions.CreateOrder(this.order));
      this.orderForm.reset();
    }
  }

  setMessage(c: AbstractControl): string | null {
    if ((c.touched || c.dirty) && c.errors) {
      return Object.keys(c.errors).map(
        key => this.validationMessages[key]).join(', ');
    }
    return null;
  }
}
