import { Pipe, PipeTransform } from '@angular/core';
import { CurrencySymbol } from './currency/currency';

@Pipe({
  name: 'productCurrency'
})
export class CurrencyPipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    return CurrencySymbol[value];
  }

}
