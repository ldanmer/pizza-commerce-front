import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductSliderComponent } from '../product/product-slider/product-slider.component';
import { BulletComponent } from './bullet/bullet.component';

@NgModule({
  declarations: [
    ProductSliderComponent,
    BulletComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ProductSliderComponent,
    BulletComponent
  ]
})
export class HomeModule { }
