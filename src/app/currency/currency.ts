export interface Currency {
  key: string;
  value: number;
}

export enum CurrencySymbol {
  USD = '$',
  EUR = '€'
}
