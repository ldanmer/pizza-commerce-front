import { Component, OnInit } from '@angular/core';
import * as fromCurrency from './state';
import * as currencyActions from './state/currency.actions';
import { Store } from '@ngrx/store';
import { CurrencySymbol } from './currency';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: [ './currency.component.scss' ]
})
export class CurrencyComponent implements OnInit {
  currency = 'EUR';

  constructor(private store: Store<fromCurrency.State>) {
  }

  ngOnInit(): void {

  }

  setCurrency($event: MouseEvent) {
    // @ts-ignore
    this.currency = $event.target.innerText;
    this.store.dispatch(new currencyActions.Load(this.currency));
  }
}
