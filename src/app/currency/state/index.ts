/* NgRx */
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CurrencyState } from './currency.reducer';
import * as fromRoot from '../../state/app.state';
import * as fromCurrency from './currency.reducer';

export interface State extends fromRoot.State {
  currency: fromCurrency.CurrencyState;
}

// Selector functions
const getProductFeatureState = createFeatureSelector<CurrencyState>('currency');

export const getCurrentCurrency = createSelector(
  getProductFeatureState,
  state => state.current
);
