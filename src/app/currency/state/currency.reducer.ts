import { Currency } from '../currency';

/* NgRx */
import { CurrencyActions, CurrencyActionTypes } from './currency.actions';

export interface CurrencyState {
  current: Currency;
  error: string;
}

const initialState: CurrencyState = {
  current: {
    key: 'EUR',
    value: 1
  },
  error: ''
};

export function reducer(state = initialState, action: CurrencyActions): CurrencyState {
  switch (action.type) {
    case CurrencyActionTypes.LoadSuccess:
      return {
        ...state,
        current: action.payload,
        error: ''
      };

    case CurrencyActionTypes.LoadFail:
      return {
        ...state,
        current: null,
        error: action.payload
      };

    default:
      return state;
  }
}
