/* NgRx */
import { Action } from '@ngrx/store';
import { Currency } from '../currency';

export enum CurrencyActionTypes {
  Load = '[Currency] Load',
  LoadSuccess = '[Currency] Load Success',
  LoadFail = '[Currency] Load Fail',
}

export class Load implements Action {
  readonly type = CurrencyActionTypes.Load;

  constructor(public payload: string) {
  }
}

export class LoadSuccess implements Action {
  readonly type = CurrencyActionTypes.LoadSuccess;

  constructor(public payload: Currency) {
  }
}

export class LoadFail implements Action {
  readonly type = CurrencyActionTypes.LoadFail;

  constructor(public payload: string) {
  }
}

export type CurrencyActions = Load
  | LoadSuccess
  | LoadFail;
