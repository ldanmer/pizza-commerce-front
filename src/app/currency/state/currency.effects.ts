import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';

/* NgRx */
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as currencyActions from './currency.actions';
import { CurrencyService } from '../currency.service';
import { Load } from './currency.actions';

@Injectable()
export class CurrencyEffects {

  constructor(private customerService: CurrencyService,
              private actions$: Actions) {
  }

  @Effect()
  loadCustomer$: Observable<Action> = this.actions$.pipe(
    ofType<Load>(currencyActions.CurrencyActionTypes.Load),
    map((action: currencyActions.Load) => action.payload),
    mergeMap((email: string) =>
      this.customerService.getCurrent(email).pipe(
        // @ts-ignore
        map(currency => (new currencyActions.LoadSuccess(currency))),
        catchError(err => of(new currencyActions.LoadFail(err)))
      )
    )
  );
}
