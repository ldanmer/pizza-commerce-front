import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyComponent } from './currency.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './state/currency.reducer';
import { EffectsModule } from '@ngrx/effects';
import { CurrencyEffects } from './state/currency.effects';
import { CurrencyPipe } from '../currency.pipe';

@NgModule({
  declarations: [
    CurrencyComponent,
    CurrencyPipe
  ],
  exports: [
    CurrencyComponent,
    CurrencyPipe,
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature('currency', reducer),
    EffectsModule.forFeature(
      [ CurrencyEffects ]
    ),
  ]
})
export class CurrencyModule { }
