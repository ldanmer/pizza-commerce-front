import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Currency } from './currency';

@Injectable({
  providedIn: 'root',
})
export class CurrencyService {
  private customersUrl = `${ environment.backendUrl }/options`;

  constructor(private http: HttpClient) {
  }

  getCurrent(key: string): Observable<Currency | Observable<never>> {
    return this.http.get<Currency | Observable<never>>(`${this.customersUrl}/${key}`)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(err) {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `${ err.error.message }`;
    } else {
      errorMessage = `Server error: ${JSON.stringify(err.error)}`;
    }
    return throwError(errorMessage);
  }
}
