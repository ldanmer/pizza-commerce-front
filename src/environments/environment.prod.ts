export const environment = {
  production: true,
  backendUrl: 'https://still-fortress-13598.herokuapp.com/api',
  imageUrl: 'https://still-fortress-13598.herokuapp.com/',
  PORT: 80
};
